#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    git clone https://github.com/Perl/perl5
    cd perl5
    git reset --hard $version
    git log -n1
}

build()
{
    cd win32
    # fix windows vs msys2 rename command
    sed -e 's/rename ..\\config.sh.tmp config.sh/mv ..\\config.sh.tmp ..\\config.sh/' -i Makefile
    cmd /c "vcvarsall.bat x86_arm64 && nmake CCTYPE=MSVC142 test-prep"
}

test()
{
    cd ../t
    file perl.exe
    ./perl.exe --version
    # remove test, brings noise
    rm porting/authors.t
    sed -i '/^t\/porting\/authors.t/d' ../MANIFEST
    # remove test, brings noise
    rm porting/copyright.t
    sed -i '/^t\/porting\/copyright.t/d' ../MANIFEST
    # remove test, brings noise
    rm porting/cmp_version.t
    sed -i '/^t\/porting\/cmp_version.t/d' ../MANIFEST
    # disable some tests in op/stat.t, as tty is not available on gitlab
    export PERL_SKIP_TTY_TEST=1
    # do not stop on failing test
    ./perl.exe harness || true
    cd ../
}

cpan_packages()
{
    target=$1

    # perl expect to be on PATH
    export PATH="$target/bin:$PATH"

    # to avoid issue (lib conflict), create a temp directory
    mkdir install_cpan_packages
    cd install_cpan_packages

    which perl.exe
    perl.exe -V:'install.*'
    # install cpan + cpanm

    # cleanup local folders
    rm -rf $HOME/.cpan $HOME/perl5
    # install cpan packages system wide (first config asks for it)
    echo -e "yes\nsudo" | perl.exe -MCPAN -e 'install CPAN' # add cpan.bat

    cpan="$(cygpath -w $target/bin/cpan)"
    cmd.exe /c "perl.exe $cpan App::cpanminus"
    cpanm="$(cygpath -w $target/bin/cpanm)"
    cmd.exe /c "perl.exe $cpanm --version"
    # install all packages
    cp -f "$script_dir/cpan_packages.txt" $target/cpan_packages.txt
    packages=$(cat "$target/cpan_packages.txt" |
               grep -v '^#' |
               sort -u)
    for package in $packages; do
        cmd.exe /c "perl.exe $cpanm -v --notest $package"
    done
}

package()
{
    # by default, perl install stuff there, could be edited at configure step
    target=/c/perl/
    rm -rf $target
    mkdir -p $target/bin
    mkdir -p $target/lib

    # copy perl.exe + dll and other exe
    cp -fv *.exe *.dll $target/bin
    # perl needs those files for installing packaging (pl2bat, ...)
    cp -fv win32/bin/* $target/bin
    rsync -a lib/ $target/lib/

    if [ -v SKIP_CPAN_PACKAGES ]; then
        echo "skip install cpan packages"
    else
        cpan_packages $target
    fi

    mv $target $out_dir
}

checkout
build
test
package
